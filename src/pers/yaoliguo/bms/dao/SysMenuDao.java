package pers.yaoliguo.bms.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.entity.SysRoleMenuKey;
@Repository("sysMenuDao")
public interface SysMenuDao {
	
    int deleteByPrimaryKey(String id);

    int insert(SysMenu record);

    int insertSelective(SysMenu record);

    SysMenu selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysMenu record);

    int updateByPrimaryKey(SysMenu record);
    
    int selectCount(Map map);
    
    List<SysMenu> selectAll(Map map);
    
    List<SysMenu> selectByIds(List<SysRoleMenuKey> list);
}