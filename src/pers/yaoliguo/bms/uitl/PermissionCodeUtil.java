package pers.yaoliguo.bms.uitl;

/**
 * @ClassName:       PermissionCodeUtil
 * @Description:    权限编码类，该类权限编码就是跳转的url
 * @author:            yao
 * @date:            2017年7月23日        上午10:53:26
 */
public interface PermissionCodeUtil {
	
	public static final String MENU = "/SysMenuRole/skipMenuRolePage";
	
	public static final String USER = "/UserControl/skipUserPage";
	
	public static final String Organize = "/SysOrgControl/skiporganizPage";
	
	public static final String ROLE = "/SysRoleControl/skipRolePage";
	
	public static final String ROLE_MENU = "/SysMenuRole/skipMenuRolePage";
}
