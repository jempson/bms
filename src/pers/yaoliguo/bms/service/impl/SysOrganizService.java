package pers.yaoliguo.bms.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pers.yaoliguo.bms.dao.SysOrganizDao;
import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.entity.SysOrganiz;
import pers.yaoliguo.bms.entity.SysRole;
import pers.yaoliguo.bms.service.ISysOrganizService;
import pers.yaoliguo.bms.uitl.MapUtils;

/**
 * @ClassName:       SysLogService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月5日        下午8:33:08
 */
@Service("sysOrganizService")
@Transactional
public class SysOrganizService implements ISysOrganizService {
    @Autowired
    SysOrganizDao sysOrganizDao;
	
	@Override
	public int deleteByPrimaryKey(String id) {
		// TODO Auto-generated method stub
		return sysOrganizDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysOrganiz record) {
		// TODO Auto-generated method stub
		return sysOrganizDao.insert(record);
	}

	@Override
	public int insertSelective(SysOrganiz record) {
		// TODO Auto-generated method stub
		return sysOrganizDao.insert(record);
	}

	@Override
	public SysOrganiz selectByPrimaryKey(String id) {
		// TODO Auto-generated method stub
		return sysOrganizDao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysOrganiz record) {
		// TODO Auto-generated method stub
		return sysOrganizDao.updateByPrimaryKeySelective(record);
	}
    
	
	@Override
	public int updateByPrimaryKey(SysOrganiz record) {
		// TODO Auto-generated method stub
		return sysOrganizDao.updateByPrimaryKey(record);
	}

	@Override
	public List<SysOrganiz> selectAll(SysOrganiz m) {
		// TODO Auto-generated method stub
		Map map = MapUtils.returnMap(m);
		return sysOrganizDao.selectAll(map);
	}
    public List<SysOrganiz> selectChildren(SysOrganiz record){
		
	    SysOrganiz r = new SysOrganiz();
		r.setPid(record.getId());
		r.setDel(false);
		
		List<SysOrganiz> list = selectAll(r);
		List<SysOrganiz> list1 = null;
		List<SysOrganiz> listCount = new ArrayList<SysOrganiz>(); 
		if(list.size() > 0){
			listCount.addAll(list);
			for (SysOrganiz sysOrganiz : list) {
				list1 = selectChildren(sysOrganiz);
				if(list1.size() > 0)
				{
					sysOrganiz.getChildren().addAll(list1);
				}
			}
		}
		
		return listCount;
	}

	@Override
	public List<SysOrganiz> selectOrganizsByKey(SysOrganiz record) {
		// TODO Auto-generated method stub
		return selectChildren(record);
	}

	@Override
	public int removeChildren(SysOrganiz record) {
		// TODO Auto-generated method stub
		List<SysOrganiz> list = selectChildren(record);
		for (SysOrganiz sysOrg : list) {
			sysOrg.setDel(true);
			sysOrganizDao.updateByPrimaryKeySelective(sysOrg);
		}
		
		return list.size();
	 
	}
	 

	
	
}
