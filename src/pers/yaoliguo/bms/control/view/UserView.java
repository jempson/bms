package pers.yaoliguo.bms.control.view;

import pers.yaoliguo.bms.entity.SysUser;

/**
 * @ClassName:       UserView
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月25日        下午7:31:28
 */
public class UserView extends SysUser{
	
	private String code;
    
	private String keyword;
	
	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	

}
