/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : bms

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-08-02 16:56:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(50) NOT NULL,
  `pid` varchar(50) DEFAULT NULL,
  `menuName` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `seq` int(50) DEFAULT NULL COMMENT '菜单同级排序号',
  `state` tinyint(1) DEFAULT '0' COMMENT '1启用，0未启用',
  `del` tinyint(1) DEFAULT '1' COMMENT '1已删除，0未删除,是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('267a408b752b436f89bcecf870e98cea', '-1', '工作流管理', 'el-icon-document', '', null, '1', '1');
INSERT INTO `sys_menu` VALUES ('5f9159d7abd64f6495fd4f0add43cedc', '8ca1d5c1c050435093990d44ee114b3d', '权限赋予', 'el-icon-message', '/SysMenuRole/skipMenuRolePage', '4', '1', '0');
INSERT INTO `sys_menu` VALUES ('68664e01782a40cda83542045c464d1f', '8ca1d5c1c050435093990d44ee114b3d', '角色管理', 'el-icon-message', '/SysRoleControl/skipRolePage', '2', '1', '0');
INSERT INTO `sys_menu` VALUES ('6f3b4424737d445ca23608afb30f1514', '8ca1d5c1c050435093990d44ee114b3d', '用户管理', 'el-icon-message', '/UserControl/skipUserPage', '3', '1', '0');
INSERT INTO `sys_menu` VALUES ('71a847bd0af448569adea9ad5a3c93d0', '-1', '流程管理', 'el-icon-date', '', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('7b32fe75f1e348da996492e595579a1b', '8ca1d5c1c050435093990d44ee114b3d', '菜单管理', 'el-icon-message', '/SysMenuControl/skipMenuPage', '1', '1', '0');
INSERT INTO `sys_menu` VALUES ('8ca1d5c1c050435093990d44ee114b3d', '-1', '系统管理', 'el-icon-setting', '', '1', '1', '0');
INSERT INTO `sys_menu` VALUES ('95d455d3de3e49a6b28da38d344cd45f', '8ca1d5c1c050435093990d44ee114b3d', '组织管理', 'el-icon-message', '/SysOrgControl/skiporganizPage', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('edcce1b978d4427fad9707793a3101b9', '267a408b752b436f89bcecf870e98cea', '已部署的流程', 'el-icon-date', '/process/getDeploy', null, '1', '1');
INSERT INTO `sys_menu` VALUES ('f130a4e54fc14fd6bebcc974835a703d', '71a847bd0af448569adea9ad5a3c93d0', '编辑流程', 'el-icon-circle-check', '/views/activity/modeler.html?modelId=1', null, '1', '0');
